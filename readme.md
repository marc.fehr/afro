# Installation

Install nvmrc and switch to correct node version with

```nvm use```

Then install packages with

```yarn install```

# Start on iOS simulator

```
cd ~/AFRO/ios/
pod install
cd ..
yarn react-native run-ios
```

# REST API documentation

https://github.com/fijal/afro/blob/master/doc/api.rst

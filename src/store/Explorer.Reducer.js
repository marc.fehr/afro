import { combineReducers } from 'redux';
import {
	BLOCK_SET_ID,
	BLOCK_CHANGE_SECTOR_ID,
	BLOCK_CHANGE_POSITION,
	BLOCK_CHANGE_DESCRIPTION,
	BLOCK_CHANGE_NAME,
	RESET_EXPLORER
} from './types';

const INITIAL_STATE = {
	block: {
		id: null,
		name: '',
		description: '',
		lat: null,
		lon: null
	}
};

const explorerReducer = (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case BLOCK_SET_ID: {
			const {
				block
			} = state;

			const uniqueID = {
				block: {
					...block,
					id: action.payload
				}
			};

			console.log('uniqueID')
			console.log(uniqueID);

			return uniqueID;
		}

		case BLOCK_CHANGE_SECTOR_ID: {
			const {
				block
			} = state;

			const sectorID = {
				block: {
					...block,
					sector: action.payload
				}
			};

			console.log('sectorID')
			console.log(sectorID);

			return sectorID;
		}

		case BLOCK_CHANGE_POSITION: {
			const {
				block
			} = state;

			// Finally, update the redux state
			const newPosition = {
				block: {
					...block,
					lat: action.payload.lat,
					lon: action.payload.lon
				}
			};

			console.log('newPosition')
			console.log(newPosition);

			return newPosition;
		}

		case BLOCK_CHANGE_NAME: {
			const {
				block
			} = state;

			// Finally, update the redux state
			const newName = {
				block: {
					...block,
					name: action.payload
				}
			};

			console.log('newName')
			console.log(newName);

			return newName;
		}

		case BLOCK_CHANGE_DESCRIPTION: {
			const {
				block
			} = state;

			const newDescription = {
				block: {
					...block,
					description: action.payload
				}
			};

			console.log('newDescription')
			console.log(newDescription);

			return newDescription;
		}

		case RESET_EXPLORER: {
			const {
				block
			} = state;

			const emptyState = {
				block: {
					...INITIAL_STATE
				}
			};

			console.log('emptyState')
			console.log(emptyState);

			return emptyState;
		}

		default:
			return state
	}
};

export default combineReducers({
	explorer: explorerReducer
});

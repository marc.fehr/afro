import {
	BLOCK_SET_ID,
	BLOCK_CHANGE_SECTOR_ID,
	BLOCK_CHANGE_POSITION,
	BLOCK_CHANGE_DESCRIPTION,
	BLOCK_CHANGE_NAME,
	RESET_EXPLORER
} from './types';

export const setBlockID = (id) => (
	{
		type: BLOCK_SET_ID,
		payload: id,
	}
);

export const changeBlockSectorID = (id) => (
	{
		type: BLOCK_CHANGE_SECTOR_ID,
		payload: id,
	}
);

export const changeBlockPosition = (position) => (
	{
		type: BLOCK_CHANGE_POSITION,
		payload: position,
	}
);

export const changeBlockName = (name) => (
	{
		type: BLOCK_CHANGE_NAME,
		payload: name,
	}
);

export const changeBlockDescription = (description) => (
	{
		type: BLOCK_CHANGE_DESCRIPTION,
		payload: description,
	}
);

export const resetExplorer = (state) => (
	{
		type: RESET_EXPLORER,
		payload: state,
	}
);

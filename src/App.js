import React from 'react';
import {Text, StyleSheet} from 'react-native';

/* REDUX STORE */
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import explorerReducer from './store/Explorer.Reducer';
const store = createStore(explorerReducer);
/* END REDUX */

import {Router, Scene, Tabs, Actions} from 'react-native-router-flux';
import {StackViewStyleInterpolator} from 'react-navigation-stack';

import MapAddMarker from 'components/map/Mapbox.AddMarker'

/* Explorer components / screens */
import ExplorerStartScreen from 'components/explorer'
import AddCragScreen from 'components/explorer/crag/addCrag'
import AddSectorScreen from 'components/explorer/sector/addSector'
import AddBlockScreen from 'components/explorer/block/addBlock'
import AddBlockPhotoScreen from 'components/explorer/block/addBlockPhoto'
import AddProblemScreen from 'components/explorer/problem/addProblem'
import AddLineScreen from 'components/explorer/line/addLine'

/* Climber components / screens */
import ClimberStartScreen from 'components/climber'
import BlockList from 'components/climber/blockList'
import BlockMap from 'components/climber/blockMap'

/* Settings components / screens */
import SettingsStartScreen from 'components/settings';

/* Modal screen example */
import ModalScreen from './components/ModalScreen';

const TabIcon = ({selected, title}) => {
  return <Text style={{color: selected ? 'red' : 'black'}}>{title}</Text>;
};

const transitionConfig = () => ({
  screenInterpolator: StackViewStyleInterpolator.forFadeFromBottomAndroid,
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scene: {
    shadowOpacity: 1,
    shadowRadius: 3,
  },
  tabBarStyle: {
    backgroundColor: '#fff',
  },
  tabBarSelectedItemStyle: {
    backgroundColor: '#ddd',
  },
});

const App = () => {
  return (
    <Provider store={store}>
      <Router
        sceneStyle={styles.scene}
      >
        <Scene key="root">
          {/* Tab Container */}
          <Scene
            key="tabbar"
            tabs={false}
            tabBarStyle={{
              backgroundColor: '#FFFFFF'
            }}
            hideNavBar
          >
            <Tabs
              key="tabbar"
              routeName="tabbar"
              backToInitial
              showLabel={false}
              tabBarStyle={styles.tabBarStyle}
              activeBackgroundColor="white"
              inactiveBackgroundColor="rgba(0, 0, 0, 0.1)"
              hideTabBar={false}
            >
              {/* Explorer mode tab */}
              <Scene key="explorer" title="Explorer" icon={TabIcon}>
                <Scene
                  key="explorer"
                  component={ExplorerStartScreen}
                  title="Explorer mode"
                />
                <Scene
                  key="addCrag"
                  component={AddCragScreen}
                  title="Add crag"
                />
                <Scene
                  key="addSector"
                  component={AddSectorScreen}
                  title="Add sector"
                />

                <Scene
                  key="addBlock"
                  component={AddBlockScreen}
                  title="Add block"
                />

                <Scene
                  key="addProblem"
                  component={AddProblemScreen}
                  title="Add problem"
                  // back
                />
                <Scene
                  key="addLine"
                  component={AddLineScreen}
                  title="Add line"
                  // back
                />
              </Scene>
              {/* End explorer mode tab */}

              {/* Climber mode tab */}
              <Scene key="climber" title="Climber" icon={TabIcon}>
                <Scene
                  key="climber"
                  component={ClimberStartScreen}
                  title="Climber"
                  onRight={() => Actions.climberMap()}
                  rightTitle="Map"
                />
                <Scene
                  key="blockMap"
                  title="Map view"
                  component={BlockMap}
                  // back
                />
                <Scene
                  key="blockList"
                  title="List view"
                  component={BlockList}
                  // back
                />
              </Scene>
              {/* End climber mode tab */}

              {/* Settings screen */}
              <Scene key="settings" title="Settings" icon={TabIcon}>
                <Scene
                  key="settings"
                  component={SettingsStartScreen}
                  title="Settings"
                />
              </Scene>
              {/* End settings screen */}

            </Tabs>
          </Scene>

          {/* Modal screens */}
          <Scene
            key="setMapMarker"
            component={MapAddMarker}
            title="Set position on the map"
            transitionConfig={transitionConfig}
            direction="vertical"
          />
          <Scene
            key="setBlockPhoto"
            component={AddBlockPhotoScreen}
            title="Add photo to block"
            transitionConfig={transitionConfig}
            direction="vertical"
          />
          {/* End modal screens */}

        </Scene>
      </Router>
    </Provider>
  );
};

export default App;

import React, {useState} from 'react';
import colors from '../../styles/AppColors'

import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	TouchableHighlight,
	TouchableWithoutFeedback,
	Image,
} from 'react-native';

const BlockRow = (props) => {
	const [showInfo, setShowInfo] = useState(false);
	const {block, index} = props;

	return (
		<View
			key={block.id}
			style={{backgroundColor: index % 2 === 0 ? 'white' : '#F3F3F7'}}>
			<View style={styles.row}>
				<View style={styles.nameAddress}>
					<Text>{block.name}</Text>
					<Text style={styles.addressText}>ID: {block.id}</Text>
				</View>

				<View style={styles.edges}>
					<TouchableHighlight
						onPress={() => setShowInfo(!showInfo)}
						style={styles.button}
						underlayColor="#5398DC">
						<Text style={styles.buttonText}>Info</Text>
					</TouchableHighlight>
				</View>
			</View>

			{showInfo && (
				<View style={styles.info}>
					<Text>
						ID: {block.id}{'\n'}{'\n'}
						Description:{'\n'}
						{block.description}{'\n'}{'\n'}
						Lat: {block.lat}{'\n'}
						Lon: {block.lon}
					</Text>
					<Image
						source={{
							uri: block.image_url,
						}}
						style={{
							flex: 1,
							height: 100,
							resizeMode: 'contain',
						}}
					/>
				</View>
			)}
		</View>
	);
}

const styles = StyleSheet.create({
	row: {
		flexDirection: 'row',
	},
	edges: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'flex-end',
		flexDirection: 'row',
		minWidth: 50,
		paddingRight: 10
	},
	nameAddress: {
		flexDirection: 'column',
		padding: 10,
		flex: 8,
	},
	addressText: {
		color: colors.brightBlue,
	},
	button: {
		borderWidth: 1,
		borderColor: colors.brightBlue,
		borderRadius: 14,
		paddingHorizontal: 10,
		paddingVertical: 5,
		backgroundColor: '#fff',
	},
	buttonText: {
		color: '#0066CC',
		fontSize: 12,
	},
	info: {
		marginHorizontal: 10,
		marginVertical: 10,
		padding: 10,
		backgroundColor: 'white',
		borderWidth: 1,
		borderColor: colors.white
	},
});

export default BlockRow;

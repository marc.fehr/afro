import React from 'react';
import {View} from 'react-native';
import styles from '../../styles/AppStyle';

import {Actions} from 'react-native-router-flux';
import AppButton from '../ui/button';

const ClimberStartScreen = () => {
  return (
    <View style={styles.container}>
      <AppButton
        title={"Map view"}
        onPress={() => {
          Actions.blockMap()
        }}
      />
      <AppButton
        title={"List view"}
        onPress={() => {
          Actions.blockList()
        }}
      />
    </View>
  );
};

export default ClimberStartScreen;

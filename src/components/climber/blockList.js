import React, {useState, useEffect} from 'react';

import {
  View,
  StyleSheet,
  TextInput,
  FlatList,
} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

import axios from 'axios';

import BlockRow from './blockRow';

const BlockList = (props) => {
  const [search, setSearch] = useState(null);
  const [blocks, setBlocks] = useState([]);

  useEffect(() => {
    axios
      .get('http://afro.baroquesoftware.com/block/list?q=sector:0')
      .then((result) => {
        console.log(result.data.blocks);
        setBlocks(result.data.blocks);
      });
  }, [])

  return (
    <KeyboardAwareScrollView style={{padding: 0, margin: 0}}>
      <View style={{flex: 1}}>
        {/*

      <View
        style={{
          marginTop: 27,
          alignItems: 'center',
        }}>
        <Image
          source={BlockIcon}
          style={{
            width: 50,
            height: 50
          }}
        />
        <Header title={'List of all boulders'} />
      </View>

      */}

        <TextInput
          clearButtonMode={'always'}
          style={styles.input}
          placeholder="Live filter (by name)"
          onChangeText={(text) => {
            setSearch(text);
          }}
          value={search}
        />

        <FlatList
          data={blocks
            .filter((block) => {
              return (
                !search ||
                block.name
                  .toLowerCase()
                  .indexOf(search.toLowerCase()) > -1
              );
            })
            .sort((a, b) => b.price - a.price)}
          renderItem={({item, index}) => (
            <BlockRow block={item} index={index} />
          )}
          keyExtractor={(item) => item.id.toString()}
          initialNumToRender={16}
        />
      </View>

    </KeyboardAwareScrollView>
  )
}

const styles = StyleSheet.create({
  input: {
    padding: 10,
    paddingHorizontal: 20,
    fontSize: 16,
    color: '#444',
    borderBottomWidth: 1,
    borderColor: '#ddd',
    backgroundColor: '#F5F5F5',
  },
});

export default BlockList

import React, {useState, useEffect} from 'react';
import BasicMap from '../map/Mapbox.Basic';

import {
  View,
  StyleSheet,
  TextInput,
  FlatList,
  Image,
} from 'react-native';

import axios from 'axios';

import Header from '../ui/Header';
import BlockIcon from 'images/marker.png';

const BlockList = (props) => {
  const [search, setSearch] = useState(null);
  const [blocks, setBlocks] = useState([]);

  const filteredBlocks = blocks.filter((block) => {
    // console.log(block);

    return (
      !search ||
      block.name
        .toLowerCase()
        .indexOf(search.toLowerCase()) > -1
    );
  })

  /* Loads all blocks from sector 0 when component did mount */
  useEffect(() => {
    axios
      .get('http://afro.baroquesoftware.com/block/list?q=sector:0')
      .then((result) => {
        // console.log(result.data.blocks);
        setBlocks(result.data.blocks);
      });
  }, [])

  return (
    <View
      style={{
        flex: 1,
      }}>

      <TextInput
        style={styles.input}
        placeholder="Live filter"
        onChangeText={(text) => {
          setSearch(text);
        }}
        value={search}
      />

      <BasicMap
        blocks={filteredBlocks}
      />

    </View>
  )
}

const styles = StyleSheet.create({
  input: {
    padding: 10,
    paddingHorizontal: 20,
    fontSize: 16,
    color: '#444',
    borderBottomWidth: 1,
    borderColor: '#ddd',
    backgroundColor: '#F5F5F5',
  },
});

export default BlockList

import React, {useState, useEffeect, useEffect} from 'react';
import {Text, TouchableOpacity, View, Image} from 'react-native';
import Geolocation from '@react-native-community/geolocation';

/* REDUX */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeBlockPosition } from '../../store/Explorer.Actions';

/* Mapbox GL */
import MapboxGL from '@react-native-mapbox-gl/maps';
MapboxGL.setAccessToken("pk.eyJ1IjoibXJjZmhyIiwiYSI6ImNqeWx2eWx3MDBicm0zY3J0YnRla25yN2UifQ.CKfTqZTXfSZie6mJHjU0OQ\n");

/* Own basic UI components */
import AppButton from '../ui/button';

/* Used for linking between screens */
import {Actions} from 'react-native-router-flux';

import styles from '../../styles/MapStyles';
import {DEFAULT_CENTER_COORDINATE} from './utils';

import Page from './common/Page';
import Bubble from './common/Bubble';

const MapAddMarker = (props) => {
	const [mapPosition, setMapPosition] = useState({
		latitude: undefined,
		longitude: undefined,
		screenPointX: undefined,
		screenPointY: undefined,
		coordinates: DEFAULT_CENTER_COORDINATE
	});

	useEffect(() => {
		Geolocation.getCurrentPosition(
			info => {
				console.log(info.coords)
				setUserPosition([info.coords.latitude, info.coords.longitude])
			},
			error => {
				console.log(error)
			},
			{
				// timeout: 5000,
				// enableHighAccuracy: true,
			});
	}, [])

	const [userPosition, setUserPosition] = useState(DEFAULT_CENTER_COORDINATE)

	const onPressMapButton = event => {
		const {geometry, properties} = event;
		console.log('pressed on map');
		console.log(geometry);

		setMapPosition({
			latitude: geometry.coordinates[1],
			longitude: geometry.coordinates[0],
			screenPointX: properties.screenPointX,
			screenPointY: properties.screenPointY,
			coordinates: [geometry.coordinates[0], geometry.coordinates[1]]
		});
	}

	const renderAnnotation = title => {
		const coordinate = mapPosition.coordinates;

		return (
			<MapboxGL.PointAnnotation
				key={'annotation'}
				id={`annotation-${title}`}
				title={title}
				coordinate={coordinate}>

				<Image
					source={require('../../images/marker.png')}
					style={{
						flex: 1,
						resizeMode: 'contain',
						width: 25,
						height: 25
					}}/>
			</MapboxGL.PointAnnotation>
		);
	}

	const renderLastClicked = () => {
		if (mapPosition.latitude === undefined && mapPosition.longitude === undefined) {
			return (
				<Bubble
					onPress={() => Actions.pop()}
				>
					<Text>Click the map to set location{"\n"}or tap this box to go back...</Text>
				</Bubble>
			);
		} else {
			return (
				<Bubble>
					<Text>Latitude: {mapPosition.latitude}</Text>
					<Text>Longitude: {mapPosition.longitude}</Text>
					<Text>Screen Point X: {mapPosition.screenPointX}</Text>
					<Text>Screen Point Y: {mapPosition.screenPointY}</Text>

					<AppButton
						title={"Save position"}
						onPress={() => {
							props.changeBlockPosition({lat: mapPosition.latitude, lon: mapPosition.longitude});
							Actions.pop();
						}}
					/>
				</Bubble>
			);
		}
	}

	return (
		<Page {...props}>
			<MapboxGL.MapView
				style={styles.matchParent}
			  onPress={onPressMapButton}
				styleURL={MapboxGL.StyleURL.Satellite}
			>
				{userPosition &&
				<MapboxGL.Camera
					centerCoordinate={userPosition}
					zoomLevel={16}
				/>
				}
				{renderAnnotation()}
			</MapboxGL.MapView>
			{renderLastClicked()}
		</Page>
	);
}

const mapStateToProps = (state) => {
	const { explorer } = state
	return { explorer }
};

const mapDispatchToProps = dispatch => (
	bindActionCreators({
		changeBlockPosition,
	}, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(MapAddMarker);

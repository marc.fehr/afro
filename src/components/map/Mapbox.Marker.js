import React from 'react';
import {Image} from 'react-native';

import MapboxGL from "@react-native-mapbox-gl/maps";
// MapboxGL.setAccessToken("pk.eyJ1IjoibXJjZmhyIiwiYSI6ImNqeWx2eWx3MDBicm0zY3J0YnRla25yN2UifQ.CKfTqZTXfSZie6mJHjU0OQ\n");

import styles from '../../styles/MapStyles';

const MapMarker = props => {
	const coordinate = [props.block.lon, props.block.lat];

	return (
		<MapboxGL.PointAnnotation
			key={'annotation'}
			id={`annotation-${props.block.id}`}
			title={props.block.title}
			coordinate={coordinate}>

			<Image
				source={require('../../images/marker.png')}
				style={{
					flex: 1,
					resizeMode: 'contain',
					width: 25,
					height: 25
				}}/>
		</MapboxGL.PointAnnotation>
	);
}

export default MapMarker

import PropTypes from 'prop-types';

const BaseExamplePropTypes = {
	label: PropTypes.string,
	onDismissExample: PropTypes.func,
	eventHandler: PropTypes.func,
	title: PropTypes.string
};

export default BaseExamplePropTypes;

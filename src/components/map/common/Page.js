import React from 'react';
import {View} from 'react-native';

import styles from '../../../styles/MapStyles';
import colors from '../../../styles/MapColors';

import BaseExamplePropTypes from './BaseExamplePropTypes';

class Page extends React.Component {
	static propTypes = {
		...BaseExamplePropTypes,
	};

	render() {
		return (
			<View style={{...styles.matchParent, width: '100%'}}>
				{this.props.children}
			</View>
		);
	}
}

export default Page;

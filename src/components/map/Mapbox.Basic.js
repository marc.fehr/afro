import React, {useEffect, useState} from 'react';
import Geolocation from '@react-native-community/geolocation';


import MapboxGL from "@react-native-mapbox-gl/maps";
MapboxGL.setAccessToken("pk.eyJ1IjoibXJjZmhyIiwiYSI6ImNqeWx2eWx3MDBicm0zY3J0YnRla25yN2UifQ.CKfTqZTXfSZie6mJHjU0OQ\n");

import {DEFAULT_CENTER_COORDINATE} from './utils';

import styles from '../../styles/MapStyles';
// import Page from './common/Page';
import MapMarker from './Mapbox.Marker'

const BasicMap = (props) => {
	const mappedBlocks = props.blocks.map((block, i) => (
		<MapMarker key={`marker-${i}`} block={block} />
	))

	useEffect(() => {
		Geolocation.getCurrentPosition(info => {
			// console.log(info.coords.latitude, info.coords.longitude)
			setUserPosition([info.coords.latitude, info.coords.longitude])
		});
	}, [])

	const [userPosition, setUserPosition] = useState([])

	return (
		<MapboxGL.MapView
			styleURL={MapboxGL.StyleURL.Satellite}
			style={styles.matchParent}
		>
			{userPosition &&
			<MapboxGL.Camera
				centerCoordinate={userPosition}
				zoomLevel={10}
			/>
			}
				{mappedBlocks}
		</MapboxGL.MapView>
	)
}

export default BasicMap;

import React from 'react';
import {View, Text} from 'react-native';

import styles from '../../styles/AppStyle'

const ScreenTitle = (props) => {
	return (
		<View style={styles.borderBottomWrapper}>
			<Text style={styles.navTitle}>
				{props.text}
			</Text>
		</View>
	);
}

export default ScreenTitle;

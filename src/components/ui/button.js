import React from 'react';
import {TouchableOpacity, Text} from 'react-native';

import styles from '../../styles/AppStyle'

const AppButton = ({ onPress, title, size, backgroundColor }) => (
	<TouchableOpacity
		onPress={onPress}
		style={[
			styles.appButtonContainer,
			size === "sm" && {
				paddingHorizontal: 8,
				paddingVertical: 6,
				elevation: 6
			},
			backgroundColor && { backgroundColor }
		]}
	>
		<Text style={[styles.appButtonText, size === "sm" && { fontSize: 14 }]}>
			{title}
		</Text>
	</TouchableOpacity>
);

export default AppButton

import {TextInput} from 'react-native';
import React from 'react';

const CustomTextInput = (props) => {
	return (
		<TextInput
			{...props}
			editable
			maxLength={40}
			placeholder={props.placeholder}
			style={{
				...props.style,
				alignItems: props.multiline ? 'flex-start' : 'center',
				fontSize: 17,
				padding: 5,
				minHeight: 35
			}}
		/>
	);
}

export default CustomTextInput;

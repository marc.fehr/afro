import React from 'react';

import {Text} from 'react-native';

import HeaderStyle from '../../styles/HeaderStyle';

const Header = (props) => {
  return <Text style={HeaderStyle.iOSHeader}>
    {props.title}
  </Text>;
};

export default Header;

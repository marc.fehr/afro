import React from 'react';
import {TouchableOpacity, Text} from 'react-native';

import styles from '../../styles/AppStyle'

const Label = (props) => (
	<Text
		style={{
			...props.styles,
			...styles.label
		}}
	>
		{props.children}
	</Text>
);

export default Label

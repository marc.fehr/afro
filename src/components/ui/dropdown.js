import React from 'react';
import DropDownPicker from 'react-native-dropdown-picker';

import colors from '../../styles/AppColors'
import styles from '../../styles/AppStyle'

const Dropdown = (props) => (
	<DropDownPicker
		{...props}
		containerStyle={{height: 40, marginTop: 20}}
		style={{
			color: colors.foreground,
		}}
		itemStyle={{
			justifyContent: 'flex-start',
			width: '100%'
		}}
		dropDownStyle={{
			backgroundColor: colors.foreground,
		}}
	/>
);

export default Dropdown

import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {Actions} from 'react-native-router-flux';

const SettingsScreen = () => {
  return (
    <View style={styles.container}>
      <Text onPress={() => Actions.black()} style={styles.welcome}>
        Change your settings
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'black',
  },
});

export default SettingsScreen;

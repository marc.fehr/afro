import React from 'react';
import {Text, View, TextInput} from 'react-native';

/* Import libs to interact with online API */
import axios from 'axios';
const qs = require('querystring');

/* Used for linking between screens */
import {Actions} from 'react-native-router-flux';

/* Import customised UI components */
import CustomTextInput from '../../ui/textInput';
import ScreenTitle from '../../ui/screenTitle';
import AppButton from '../../ui/button';
import Label from '../../ui/label';

/* Import custom styles */
import styles from '../../../styles/AppStyle';

const config = {
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded'
	}
}

const AddProblemScreen = () => {
	const [cragName, changecragName] = React.useState('');
	const [cragDescription, changeCragDescription] = React.useState('');
	const [cragID, setCragID] = React.useState(null);

	/* POST request URL */
	const url = 'http://afro.baroquesoftware.com/crag/add';

	const data = {
		name: cragName,
		description: Math.random() + ' is a number',
	};

	const handleSubmit = event => {
		axios.post(url, qs.stringify(data), config)
			.then((res) => {
				console.log(res);
				setCragID(res.data.id);
			})
	}

	return (
		<View style={styles.container}>
			<ScreenTitle text={"Add new problem"} />
			<View
				style={{
					...styles.textInputContainer,
					width: '90%'
				}}
			>
				<CustomTextInput
					multiline={false}
					placeholder={'Enter name of the problem here...'}
					onChangeText={text => changecragName(text)}
					style={styles.textInput}
					value={cragName}
				/>
			</View>
			<View
				style={{
					...styles.textInputContainer,
					width: '90%'
				}}
			>
				<CustomTextInput
					multiline
					numberOfLines={4}
					placeholder={'Description'}
					onChangeText={text => changeCragDescription(text)}
					style={{
						...styles.textInput,
						height: 150,
						textAlign: 'left'
					}}
					value={cragDescription}
				/>
			</View>

			<AppButton
				title={"Add problem"}
				onPress={() => {
					if (cragID) { return } else { handleSubmit() }
				}}
			/>

			<Label>
				API Problem ID: {cragID || 'not sent to database, yet!'}
			</Label>

		</View>
	);
};

export default AddProblemScreen;

import React from 'react';
import {Text, View, TextInput} from 'react-native';

/* Import libs to interact with online API */
import axios from 'axios';
const qs = require('querystring');

/* Used for linking between screens */
// import {Actions} from 'react-native-router-flux';

/* Import customised UI components */
import CustomTextInput from '../../ui/textInput';
import ScreenTitle from '../../ui/screenTitle';
import AppButton from '../../ui/button';
import Label from '../../ui/label';
import Dropdown from '../../ui/dropdown';

/* Import custom styles */
import styles from '../../../styles/AppStyle';
import colors from '../../../styles/AppColors';

/* Static data instead of API fetch for now */
const allCrags = [
	{label: 'Rocklands', value: 1},
	{label: 'Topside', value: 2}
];

const AddSectorScreen = () => {
	const [parent, setParent] = React.useState({cragID: null});
	const [sectorName, changesectorName] = React.useState('');
	const [sectorDescription, changesectorDescription] = React.useState('');
	const [sectorID, setsectorID] = React.useState(null);

	const data = {
		cragID: parent.cragID,
		name: sectorName,
		description: sectorDescription,
	};

	/* POST request URL and config for request headers */
	const url = 'http://afro.baroquesoftware.com/sector/add';
	const config = {
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	};

	const handleSubmit = event => {
		axios.post(url, qs.stringify(data), config)
			.then((res) => {
				console.log(res);
				setsectorID(res.data.id);
			});
	};

	return (
		<View style={styles.container}>
			<ScreenTitle text={"Add new sector"} />

			<View style={{width: '90%', zIndex: 999}}>
				<Dropdown
					defaultValue={null}
					items={allCrags}
					placeholder={'Select a crag'}
					searchable
					searchablePlaceholder="Search for an existing crag"
					onChangeItem={item => {
						setParent({
							cragID: item.value
						})
					}}
				/>
			</View>

			<View
				style={{
					...styles.textInputContainer,
					width: '90%'
				}}
			>
				<CustomTextInput
					multiline={false}
					placeholder={'Enter name of the sector here...'}
					onChangeText={text => changesectorName(text)}
					style={styles.textInput}
					value={sectorName}
				/>
			</View>
			<View
				style={{
					...styles.textInputContainer,
					width: '90%'
				}}
			>
				<CustomTextInput
					multiline
					numberOfLines={4}
					placeholder={'Description'}
					onChangeText={text => changesectorDescription(text)}
					style={{
						...styles.textInput,
						height: 150,
						textAlign: 'left'
					}}
					value={sectorDescription}
				/>
			</View>

			<AppButton
				title={"Add sector"}
				onPress={() => {
					if (sectorID || !parent.cragID) { return } else { handleSubmit() }
				}}
			/>

			<Label>
				API Sector ID: {sectorID || 'not sent to database, yet!'}{'\n'}
				{parent.cragID ? 'Crag ID: ' + parent.cragID : ''}
			</Label>

		</View>
	);
};

export default AddSectorScreen;

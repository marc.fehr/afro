import React, { useState } from 'react';
import {View, ActivityIndicator} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

/* REDUX */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
	setBlockID,
	changeBlockName,
	changeBlockDescription,
	resetExplorer,
	changeBlockSectorID
} from '../../../store/Explorer.Actions';

/* Import libs to interact with online API */
import axios from 'axios';
const qs = require('querystring');

/* Used for linking between screens */
import {Actions} from 'react-native-router-flux';

/* Import customised UI components */
import CustomTextInput from '../../ui/textInput';
import ScreenTitle from '../../ui/screenTitle';
import AppButton from '../../ui/button';
import Label from '../../ui/label';
import Dropdown from '../../ui/dropdown';

/* Import custom styles */
import styles from '../../../styles/AppStyle';
import colors from '../../../styles/AppColors';

/* Static data instead of API fetch for now */
const allCrags = [
	{label: 'Ou Poort (Rocklands)', value: 1},
	{label: 'Mutant Snail (Topside)', value: 2}
];

const AddblockScreen = (props) => {
	const data = {
		// sector: props.explorer.block.sector,
		sector: 0,
		name: props.explorer.block.name,
		description: props.explorer.block.description,
		lat: props.explorer.block.lat,
		lon: props.explorer.block.lon
	};

	const [isLoading, setIsLoading] = useState(false);

	/* POST request URL and config for request headers */
	const url = 'http://afro.baroquesoftware.com/block/add';
	const config = {
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	};

	const handleSubmit = event => {
		setIsLoading(true);
		axios.post(url, qs.stringify(data), config)
			.then((res) => {
				setTimeout(() => {
					console.log(res.data);
					setIsLoading(false);
					/* Set UID from API to Redux block state */
					props.setBlockID(res.data.id);
				}, 2000)
			});
	};

	return (
		<KeyboardAwareScrollView
			contentContainerStyle={styles.container}
		>
			{props.explorer.block.id &&
			<View style={{flex: 1, width: '90%', justifyContent: 'center', alignContent: 'center'}}>
				<ScreenTitle text={"Successfully added to API"} />
				<AppButton
					title={"Add another block"}
					onPress={() => {
						props.resetExplorer()
					}}
				/>
			</View>
			}
			{!props.explorer.block.id &&
			<View style={{flex: 1, width: '90%', justifyContent: 'center', alignContent: 'center'}}>
				<ScreenTitle
					text={"Add new block"}
				/>
				<View style={{width: '100%', zIndex: 999}}>
					<Dropdown
						defaultValue={null}
						items={allCrags}
						placeholder={'Select a sector'}
						searchable
						searchablePlaceholder="Search for an existing sector"
						onChangeItem={item => props.changeBlockSectorID(item.value)}
					/>
				</View>
				<View
					style={styles.textInputContainer}>
					<CustomTextInput
						multiline={false}
						placeholder={'Enter name of the block here'}
						onChangeText={text => props.changeBlockName(text)}
						style={styles.textInput}
						value={props.explorer.block.name}
					/>
				</View>
				<View
					style={styles.textInputContainer}>
					<CustomTextInput
						multiline
						numberOfLines={4}
						placeholder={'Description'}
						onChangeText={text => props.changeBlockDescription(text)}
						style={{
							...styles.textInput,
							height: 150,
							textAlign: 'left'
						}}
						value={props.explorer.block.description}
					/>
				</View>
				<AppButton
					title={"Set position"}
					onPress={() => {
						Actions.setMapMarker()
					}}
				/>
				<AppButton
					title={"Add photo"}
					onPress={() => {
						Actions.setBlockPhoto()
					}}
				/>
				{props.explorer.block.lat &&
					<AppButton
						style={{
							margin: 0
						}}
						title={"Add block"}
						onPress={() => {
							handleSubmit()
						}}
					/>
				}
				{isLoading &&
				<ActivityIndicator size="large" style={styles.loadingAnimation} />
				}
			</View>
			}
		</KeyboardAwareScrollView>
	);
};

const mapStateToProps = (state) => {
	const { explorer } = state
	return { explorer }
};

const mapDispatchToProps = dispatch => (
	bindActionCreators({
		setBlockID,
		changeBlockName,
		changeBlockDescription,
		resetExplorer,
		changeBlockSectorID
	}, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(AddblockScreen);

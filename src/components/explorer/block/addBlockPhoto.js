import React, { useState } from 'react';
import {View, ActivityIndicator, Text} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';

/* REDUX */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
	setBlockPhoto
} from '../../../store/Explorer.Actions';

/* Import libs to interact with online API */
import axios from 'axios';
const qs = require('querystring');

/* Used for linking between screens */
import {Actions} from 'react-native-router-flux';

/* Import customised UI components */
import AppButton from 'components/ui/button';
// import CustomTextInput from '../ui/textInput';
import ScreenTitle from 'components/ui/screenTitle';
// import Label from '../ui/label';
// import Dropdown from '../ui/dropdown';

/* Import custom styles */
import styles from '../../../styles/AppStyle';
// import colors from '../../styles/AppColors';

const AddBlockPhotoScreen = (props) => {
	const data = {
		// sector: props.explorer.block.sector,
		sector: 0,
		// Add photo specific fields
	};

	const [isLoading, setIsLoading] = useState(false);

	/* POST request URL and config for request headers */
	const url = 'http://afro.baroquesoftware.com/block/add';
	const config = {
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	};

	const handleSubmit = event => {
		setIsLoading(true);
		axios.post(url, qs.stringify(data), config)
			.then((res) => {
				setTimeout(() => {
					console.log(res.data);
					setIsLoading(false);
					/* Set UID from API to Redux block state */
					props.setBlockID(res.data.id);
				}, 2000)
			});
	};

	return (
		<View style={styles.container}>
			<ScreenTitle text={"Add photo"} />
			<AppButton
				title={"Choose from library"}
				onPress={() => {
					props.resetExplorer()
				}}
			/>
			<AppButton
				title={"Start camera"}
				onPress={() => {
					props.resetExplorer()
				}}
			/>
		</View>
	);
};

const mapStateToProps = (state) => {
	const { explorer } = state
	return { explorer }
};

const mapDispatchToProps = dispatch => (
	bindActionCreators({
		setBlockPhoto
	}, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(AddBlockPhotoScreen);

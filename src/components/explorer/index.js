import React from 'react';
import {Text, View} from 'react-native';

/* REDUX */
import { connect } from 'react-redux';

import {Actions} from 'react-native-router-flux';

/* Import customised UI components */
import ScreenTitle from '../ui/screenTitle';
import AppButton from '../ui/button';

import styles from '../../styles/AppStyle';

const ExplorerStartScreen = (props) => {

  return (
    <View style={styles.container}>
      <AppButton
        onPress={() => Actions.addCrag()}
        title={'Add a crag'}
      />
      <AppButton
        onPress={() => Actions.addSector()}
        title={'Add sector to crag'}
      />
      <AppButton
        onPress={() => Actions.addBlock()}
        title={'Add block to sector'}
      />
      <Text
        style={{
          textAlign: 'center', color: 'grey',
          paddingTop: 20
        }}
      >
        Current explorer block name is { props.explorer.block.name || 'not set, yet.'}</Text>
      <AppButton
        onPress={() => Actions.addProblem()}
        title={'Add problem to block'}
      />
      <AppButton
        onPress={() => Actions.addLine()}
        title={'Add line to problem'}
      />
    </View>
  )
};

const mapStateToProps = (state) => {
  const { explorer } = state
  return { explorer }
};

export default connect(mapStateToProps)(ExplorerStartScreen);

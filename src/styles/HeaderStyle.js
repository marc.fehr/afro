import { StyleSheet } from 'react-native'
import colors from './AppColors'

export default StyleSheet.create({
  header: {
    padding: 40,
    paddingTop: 0,
    fontSize: 30,
    textAlign: 'center',
    color: '#0066CC',
    fontWeight: '300'
  },
  iOSHeader: {
    padding: 20,
    paddingTop: 10,
    fontSize: 26,
    textAlign: 'center',
    color: colors.brightBlue,
    fontWeight: '200'
  }
})

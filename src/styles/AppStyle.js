import { StyleSheet } from 'react-native'
import colors from './AppColors'

export default StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.brightWhite,
		padding: 0,
		margin: 0
	},
	loadingAnimation: {
		padding: 10,
		paddingTop: 20
	},
	appButtonContainer: {
		elevation: 8,
		backgroundColor: colors.brightBlue,
		borderRadius: 10,
		paddingVertical: 10,
		paddingHorizontal: 12,
		marginTop: 20
	},
	appButtonText: {
		fontSize: 18,
		color: colors.brightWhite,
		fontWeight: "bold",
		alignSelf: "center",
		textTransform: "uppercase"
	},
	navLink: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
		color: colors.brightBlack,
	},
	borderBottomWrapper: {
		borderBottomColor: colors.brightBlue,
		borderBottomWidth: 2,
		marginBottom: 10
	},
	navTitle: {
		fontSize: 30,
		textAlign: 'center',
		margin: 10,
		color: colors.brightBlue
	},
	label: {
		backgroundColor: colors.white,
		padding: 10,
		width: '90%',
		borderRadius: 2,
		position: 'absolute',
		bottom: 10,
		textAlign: 'center'
	},
	textInputContainer: {
		width: '100%',
	},
	textInput: {
		backgroundColor: 'white',
		padding: 10,
		marginTop: 10,
		textAlign: 'left',
	},
	mapContainer: {
		flex: 1,
		backgroundColor: 'white',
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	}
});


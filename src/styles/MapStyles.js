import {StyleSheet} from 'react-native';

const styles = {
	matchParent: {
		flex: 1
	},
	map: {
		flex: 1,
		width: '100%',
		height: 100
	}
};

export default StyleSheet.create(styles);
